package orthophoto

import (
	"fmt"

	"bitbucket.org/3domfbk/commontools/geom"
	mttypes "bitbucket.org/3domfbk/meshtools/types"
	"bitbucket.org/3domfbk/sfmtools/types"
)

// Generate an orthophoto from a mesh and a view
func Generate(bsp *mttypes.BSPTree, view *types.View, orthophoto types.OrthoPhoto) (types.OrthoPhoto, error) {

	width := view.Image.Image.Bounds().Dx()
	height := view.Image.Image.Bounds().Dy()

	pixelSize := view.Camera.Intrinsics.SensorWidth / float64(width)

	fmt.Print("Iterating image pixels...")
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {

			pixelRay := geom.Vec3{
				X: float64(x-width/2) * pixelSize,  // TODO - add ppx
				Y: float64(y-height/2) * pixelSize, // TODO - add ppy
				Z: view.Camera.Intrinsics.FocalLength,
			}

			direction := view.Pose.RotationMatrix.VecMul(pixelRay)

			ray := geom.NewRay(view.Pose.Center, direction)

			node, t := bsp.RayTrace(ray)

			if node != nil {
				point := ray.Origin.Add(ray.Direction.Scl(t))

				orthoX := 1.0 / orthophoto.Resolution * (point.X - orthophoto.X0)
				orthoY := 1.0 / orthophoto.Resolution * (orthophoto.Y0 - point.Y)

				if int(orthoX) >= 0 && int(orthoX) < orthophoto.RGBA.Bounds().Dx() && int(orthoY) >= 0 && int(orthoY) < orthophoto.RGBA.Bounds().Dy() {
					orthophoto.RGBA.Set(int(orthoX), int(orthoY), view.Image.Image.At(x, y))
				}
			}
		}
	}

	return orthophoto, nil
}
