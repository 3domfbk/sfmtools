package main

import (
	"fmt"
	"image"
	"image/jpeg"
	"os"

	"bitbucket.org/3domfbk/commontools/geom"
	imtio "bitbucket.org/3domfbk/imagetools/io"
	mtio "bitbucket.org/3domfbk/meshtools/io"
	mttypes "bitbucket.org/3domfbk/meshtools/types"
	sfmortho "bitbucket.org/3domfbk/sfmtools/orthophoto"
	"bitbucket.org/3domfbk/sfmtools/types"
)

func main() {

	if len(os.Args) < 4 {
		fmt.Println("usage: sfmt meshFileName imageFileName outputFileName")
		os.Exit(0)
	}

	meshPath := os.Args[1]
	imagePath := os.Args[2]
	outputFileName := os.Args[3]

	fmt.Println("Generating an orthophoto...")
	intrinsics := types.Intrinsics{SensorWidth: 6.16, SensorHeight: 4.62, FocalLength: 4.27127}
	camera := types.Camera{Intrinsics: &intrinsics}

	imageView, err := imtio.ReadMetaImage(imagePath)

	if err != nil {
		os.Exit(-1)
	}

	view := types.View{
		Image: imageView,
		Pose: &types.Pose{Center: geom.Vec3{
			X: -0.7655892532928874,
			Y: 2.404253402525358,
			Z: 6.519789479482752},
			RotationMatrix: geom.Matrix4{
				Data: [4][4]float64{[4]float64{0.7227883688217373, 0.6723605709536996, 0.15971298169788057, 0},
					[4]float64{0.6680015781598488, -0.6205336973090513, -0.41074544681579869, 0},
					[4]float64{-0.17706175609646414, 0.40357055533174598, -0.8976524613664592, 0},
					[4]float64{0, 0, 0, 1}}}},
		Camera: &camera}

	mesh, err := mtio.ReadMesh(meshPath)

	if err != nil {
		os.Exit(-1)
	}

	fmt.Print("Creating the BSP...")
	bsp := mttypes.CreateBSPTree(mesh)
	fmt.Println("DONE")

	resolution := 0.05 // 5 cm

	orthoWidth := (bsp.Root.BBox.Max.X - bsp.Root.BBox.Min.X) / resolution
	orthoheight := (bsp.Root.BBox.Max.Y - bsp.Root.BBox.Min.Y) / resolution

	orthophoto := types.OrthoPhoto{
		X0:         bsp.Root.BBox.Min.X,
		Y0:         bsp.Root.BBox.Max.Y,
		RGBA:       image.NewRGBA(image.Rect(0, 0, int(orthoWidth), int(orthoheight))),
		Resolution: resolution}

	orthophoto, err = sfmortho.Generate(bsp, &view, orthophoto)

	if err == nil {
		fmt.Println("DONE")
		fmt.Print("Saving results.")
		toimg, _ := os.Create(outputFileName)

		defer toimg.Close()

		jpeg.Encode(toimg, orthophoto.RGBA, &jpeg.Options{Quality: jpeg.DefaultQuality})
	}
}
