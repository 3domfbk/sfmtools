package types

import imttypes "bitbucket.org/3domfbk/imagetools/types"

// View represents a view from the camera
type View struct {
	Camera *Camera
	Pose   *Pose
	Image  *imttypes.MetaImage
}
