package types

import "bitbucket.org/3domfbk/commontools/geom"

// Intrinsics the camera intrinsics parameters
type Intrinsics struct {
	SensorWidth, SensorHeight, FocalLength float64
}

// Pose defines a camera pose
type Pose struct {
	Center            geom.Vec3
	Omega, Phi, Kappa float64
	RotationMatrix    geom.Matrix4
}

// Camera is a camera model
type Camera struct {
	Intrinsics *Intrinsics
}
