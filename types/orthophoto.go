package types

import "image"

// OrthoPhoto defines and orthophoto
type OrthoPhoto struct {
	RGBA               *image.RGBA
	X0, Y0, Resolution float64
}
